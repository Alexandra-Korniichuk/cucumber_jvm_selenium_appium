# CucumberJVM+Selenium+Appium example project
Example project of testing a website by Using CucumberJVM, Selenium and Appium frameworks
## Getting Started
Instructions of how to run the project on your local machine
### Prerequisites
On MAC OS:
1. Install JDK - [link](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. Install Selenium - [link](http://www.seleniumhq.org/download/)
3. Install Chrome Driver using brew: `brew install chromedriver`
4. Install Geckodriver (to run tests in Firefox): `brew install geckodriver`
#### To run tests on virtual mobile devices
5. Install XCode
6. Install Node.js and npm [download and install](https://www.npmjs.com/get-npm) or install it by using brew: `brew install node`
7. Download and install [Appium app for desktop](https://github.com/appium/appium-desktop/releases ) or install it by using npm: `npm install -g appium`
8. To verify that all Appium’s dependencies are met install appium-doctor by using npm: `npm install -g appium-doctor`. 

After appium-doctor was installed run `appium-doctor —ios` and `appium-doctor --android`.

_If any problems were found, please, fix them._

**Some of the most popular problems:**

* _Err: Carthage was NOT found._ 

    Installation information can be found [there](https://github.com/Carthage/Carthage#installing-carthage)
* _Err: JAVA_HOME is NOT set._ 

    Open bash_profile with nano editor: `nano ~/.bash_profile`
    
	Add to file:
	```
	export JAVA_HOME=$(/usr/libexec/java_home) 
	export PATH=$JAVA_HOME/bin:$PATH
	```
	
	Save changes and exit. To reload type `source .bash_profile` and then check your changes by using `echo $JAVA_HOME`
	 
* _Err: ANDROID_HOME is NOT set._

	Download and install [Android Studio](https://developer.android.com/studio/index.html) and then run it and follow the instructions
	
	In terminal open bash_profile with nano editor `nano ~/.bash_profile`
	 
	Add to the file:
	```
	export ANDROID_HOME=/{YOUR_PATH_TO_Android_SDK}/ 
	export PATH=$ANDROID_HOME/platform-tools:$PATH
	export PATH=$ANDROID_HOME/tools:$PATH
	```
	
	Save changes and exit. To reload type `source .bash_profile` and then check your changes by using `echo $ANDROID_HOME`
### Running the tests
You can run tests by using following commands:

`mvn test -Dbrowser=chrome` - to run tests in Chrome

`mvn test -Dbrowser=firefox` - to run tests in Firefox 

`mvn test -Dbrowser=android` - to run tests on Android device

`mvn test -Dbrowser=ios` - to run tests on iOS device

To run tests on mobile devices, run appium from terminal: `appium -a 127.0.0.1` OR run it by using Appium desktop app (HOST: 127.0.0.1 PORT:4723).

_To run tests on iOS virtual device:_ enter `xcrun simctl list` command in terminal. From the list copy device type id and runtime id.
Create iOS simulator by using: `xcrun simctl create <name> <device type id> <runtime id>` command

_To run tests on Android virtual device:_ create and run Android virtual device by using Android SDK [link with instructions](https://developer.android.com/studio/run/managing-avds.html)
## Build With
* [Maven](https://maven.apache.org/) - Dependency management
* [Selenium](http://www.seleniumhq.org) - Software testing framework for web applications 
* [Cucumber](https://cucumber.io/) - BDD testing framework
* [Appium](http://appium.io) - Open source test automation framework for use with native, hybrid and mobile web apps.





