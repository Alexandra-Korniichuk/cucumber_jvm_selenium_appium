package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.LandingPage;
import utils.DriverFactory;

public class LandingPageSteps {

    private DriverFactory factory;

    private LandingPage landingPage;

    @Before
    public void beforeScenario() throws Exception {
        factory = new DriverFactory();
        landingPage = new LandingPage(factory.getDriver());
    }

    @After
    public void afterScenario() {
        factory.destroyDriver();
    }

    @Given("landing page is opened")
    public void setup() {
        landingPage.openHomePage();
    }

    @When("a user clicks Contact Us link")
    public void click_contact_us_link() {
        landingPage.clickContactUsLink();
    }

    @Then("Contact form should be opened")
    public void verify_that_contact_form_is_opened() {
        landingPage.waitForContactPopUp();
        landingPage.verifyContactUsFormDisplayed();
    }
}
