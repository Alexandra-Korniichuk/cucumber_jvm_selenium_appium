@ContactUs
Feature:
  As a client I want the ability to ask about products through contact form

  @ContactUs-OpenForm
  Scenario: Open contact form from landing page
    Given landing page is opened
    When a user clicks Contact Us link
    Then Contact form should be opened