package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LandingPage {

    private WebDriver driver;

    //Main menu button(hamburger button)
    @FindBy(id = "nav-switcher")
    WebElement mainMenuButton;

    //Navigation links
    @FindBy(id = "contact-us")
    WebElement contactUsLink;

    @FindBy(id = "contact-us-mobile")
    WebElement contactUsMobLink;

    //Contact Us popup and it's elements
    @FindBy(className = "content-info")
    WebElement contactPopUp;

    @FindBy(id = "contactForm")
    WebElement contactUsForm;

    public LandingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openHomePage() {
        driver.get("https://ratel.io");
    }

    /**
     * If the site is opened in mobile browser,
     * click hamburger menu button and then Contact Us link.
     * If the site is opened in desktop browser, click Contact Us link
     */
    public void clickContactUsLink() {
        if (driver instanceof MobileDriver) {
            mainMenuButton.click();
            contactUsMobLink.click();
        } else {
            contactUsLink.click();
        }
    }

    public void waitForContactPopUp() {
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOf(contactPopUp));
    }

    /**
     * Verify that Contact Us form displayed
     */
    public void verifyContactUsFormDisplayed() {
        Assert.assertTrue((contactUsForm).isDisplayed());
    }
}
