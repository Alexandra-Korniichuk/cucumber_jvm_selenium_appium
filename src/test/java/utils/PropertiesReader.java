package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    public Properties properties = new Properties();
    InputStream inputStream = null;

    PropertiesReader() {
        loadProperties();
    }

    private void loadProperties() {
        try {
            inputStream = getClass().getResourceAsStream("/config.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String get(String key) {
        return properties.getProperty(key);
    }
}

