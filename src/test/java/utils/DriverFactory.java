package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;

public class DriverFactory {

    private WebDriver driver;


    public DriverFactory() throws Exception {
        createNewDriverInstance();
    }

    /**
     * Creates new driver instance by getting browser property
     * Default browser property is Chrome
     * Other available properties are: Firefox, Android and iOS
     * @throws Exception when incorrect browser name was received
     */
    private void createNewDriverInstance() throws Exception {
        String browserType = System.getProperty("browser", "chrome");
        if ("firefox".equals(browserType)) {
            driver = new FirefoxDriver();
        } else if ("chrome".equals(browserType)) {
            driver = new ChromeDriver();
        } else if ("android".equals(browserType)) {
            DesiredCapabilities capabilitiesAndroid = androidDriverSetUp();
            URL url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AppiumDriver(url, capabilitiesAndroid);
        } else if ("ios".equals(browserType)) {
            DesiredCapabilities capabilitiesIOS = iosDriverSetUp();
            URL url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AppiumDriver(url, capabilitiesIOS);
        } else {
            throw new RuntimeException("Unsupported browserType: " + browserType);
        }
    }

    private DesiredCapabilities androidDriverSetUp(){
        PropertiesReader properties = new PropertiesReader();
        String platformVersion = properties.get("android_version");
        String deviceName = properties.get("android_device");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
        return capabilities;
    }

    private DesiredCapabilities iosDriverSetUp(){
        PropertiesReader properties = new PropertiesReader();
        String platformVersion = properties.get("ios_version");
        String deviceName = properties.get("ios_device");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        return capabilities;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        driver.quit();
    }
}